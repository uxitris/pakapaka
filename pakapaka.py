# -*- coding: utf-8 -*-
# Módulo: Paka Paka
# Autor: Cristian P. Fusaro
# Enero 2021
# Versión 0.2.0
# Requiere:
#   - Python >= 3
#   - Kodi >= 19


import os
import sys
import xbmcaddon
import xbmcgui
import xbmcplugin
import requests
from bs4 import BeautifulSoup
import urllib.request, urllib.parse, urllib.error
#from urllib.parse import urlencode
from urllib.parse import parse_qsl

# Variables globales

# url del complemento (plugin://plugin.video.pakapaka)
plugin_url = sys.argv[0]
# Identificador del complemento
ID = int(sys.argv[1])


def main():
    args = parse_qsl(sys.argv[2][1:])
    # Crea un diccionario con los argumentos que se reciben en el URL cada llamada
    params = dict(args)
    if params:
        if params['action'] == 'lista':
            # Lista los videos del minisitio
            listarVideos(params['categoria'])
        elif params['action'] == 'reproducir':
            # Reproduce el video seleccionado en el URL
            reproducirVideo(params['video'])
        else:
            raise ValueError('¡Parámetros inválidos: {0}!'.format(params))         
    else:
        # Cuando se llama inicialmente al complemento sin parámetros,
        # se muestra la lista de Minisitios
        listarMinisitios()


def urlPlugin(**kwargs):
    """
    Crea un URL para llamar al complemento recursivamente a partir de los parámetros.

    :param kwargs: pares de "clave=valor"
    :type kwargs: diccionario
    :return: URL del complemento en notación plugin://...
    :rtype: string
    """
    return '{0}?{1}'.format(plugin_url, urllib.parse.urlencode(kwargs))


def obtenerMinisitios():
	"""
	Crea un diccionario con la info de los minisitios encontrados en la página principal del sitio Paka Paka
	
	:return: diccionario de minisitios
	"""
	minisitios = {}
	pag = requests.get("http://www.pakapaka.gob.ar/videos/")
	minestrone = BeautifulSoup(pag.content, 'html.parser')
	res = minestrone.find(id='owl-carousel')
	ms = res.find_all('li')
	for sitio in ms:
		direccion = sitio.find('a')['href']
		miniatura = sitio.find('img')['data-src']
		nombre = sitio.text.strip()
		if "minisitios" in direccion:
			enlace = "http://www.pakapaka.gob.ar" + direccion
		else:
			# Saltamos los sitios que no son de videos. No son minisitios.
			continue
		minisitios.update({nombre : [miniatura, enlace]})

	return minisitios


def listarMinisitios():
    """
    Crea la lista de minisitios en la interfaz de Kodi.
    """
    xbmcplugin.setPluginCategory(ID, 'Programas')
    xbmcplugin.setContent(ID, 'videos')
    # Obtiene los minisitios
    minisitios = obtenerMinisitios()
    for nombreSitio in minisitios:
        listaDeMinisitios = xbmcgui.ListItem(label=nombreSitio)
        # fanart por defecto del complemento para todos los minisitios
        listaDeMinisitios.setArt({'thumb': minisitios[nombreSitio][0],
                          'icon': minisitios[nombreSitio][0],
                          'fanart': xbmcaddon.Addon('plugin.video.pakapaka').getAddonInfo('fanart')})
        listaDeMinisitios.setInfo('video', {'title': nombreSitio, 'mediatype': 'video'})
        # Crea el URL para la llamada recursiva al complemento.
        # Ejemplo: plugin://plugin.video.pakapaka/?action=lista&categoria=Robotia
        url = urlPlugin(action='lista', categoria=nombreSitio)
        is_folder = True
        xbmcplugin.addDirectoryItem(ID, url, listaDeMinisitios, is_folder)

    xbmcplugin.addSortMethod(ID, xbmcplugin.SORT_METHOD_LABEL)
    xbmcplugin.endOfDirectory(ID)
    
    
def obtenerVideos(sitio):
	videos = []
	minisitios = obtenerMinisitios()
	URL = minisitios[sitio][1]
	pag = requests.get(URL)
	puchero = BeautifulSoup(pag.content, 'html.parser')
	res = puchero.find("div", {"class":"resultados centro"})
	# Hay minisitios con diferentes <div>
	# Alta Noticia
	if 'None' in (str(res)): 
		res = puchero.find("div", {"class":"resultados centro resultadosAltaNoticia"})
	# Puerto Papel
	if 'None' in (str(res)): 
		res = puchero.find("div", {"class":"resultados centro resultadosPuertoPapel"})
	# si no lo encontramos, no devolvemos videos
	if 'None' in (str(res)):
		return videos
	ms = res.find_all('li')
	for video in ms:
		# No todas las páginas tienen solo videos, hay que analizar
		# el tipo de contenido. Solo queremos "Video"
		tieneContenido = video.find('p')
		if 'None' in (str(tieneContenido)):
			continue
		tipoDeContenido = tieneContenido.text.strip()
		if tipoDeContenido == "Video":
			direccion = video.find('a')['href']
			miniatura = video.find("div", {"class":"responsiveThumb ratio16-9"})['style']
			com = miniatura.find("http")
			fin = miniatura.find('"', com + 1)
			nMiniatura = miniatura[com:fin]
			nombre = video.find('h3').text.strip()
			# No todos los videos siguen el mismo esquema de "/video/número"
			if "videos" in direccion:
				enlace = "http://www.pakapaka.gob.ar" + direccion
			else:
				enlace = direccion	
			videos.append({'nombre':nombre, 'enlace':enlace, 'miniatura':nMiniatura})
		
	return videos


def listarVideos(categoria):
    """
    Crea una lista de videos en la interfaz de Kodi.

    :param categoria: nombre del minisitio (Serie de Paka Paka)
    :type categoria: string
    """
    xbmcplugin.setPluginCategory(ID, categoria)
    xbmcplugin.setContent(ID, 'videos')
    videos = obtenerVideos(categoria)
    for video in videos:
        listaDeVideos = xbmcgui.ListItem(label=video['nombre'])
        listaDeVideos.setInfo('video', {'title': video['nombre'], 'mediatype': 'video'})
        listaDeVideos.setArt({'thumb': video['miniatura'], 'icon': video['miniatura'], 'fanart': video['miniatura']})
        listaDeVideos.setProperty('IsPlayable', 'true')
        # Crea el URL para la llamada recursiva al complemento.
        # Ejemplo: plugin://plugin.video.pakapaka/?action=reproducir&video=http://bla/ble/bli
        url = urlPlugin(action='reproducir', video=video['enlace'])
        is_folder = False
        xbmcplugin.addDirectoryItem(ID, url, listaDeVideos, is_folder)
    xbmcplugin.addSortMethod(ID, xbmcplugin.SORT_METHOD_UNSORTED)
    xbmcplugin.endOfDirectory(ID)


def reproducirVideo(sitio):
    """
    Obtiene el enlace al video dentro del sitio pasado por parámetro y
    lo pasa al reproductor de Kodi.

    :param sitio: enlace al sitio donde está el video
    :type sitio: string
    """
    
    # Obtiene la página del video
    pag = requests.get(sitio)
    gazpacho = BeautifulSoup(pag.content, 'html.parser')
    enlaceVideo = (gazpacho.find(id="player_frame")['src'])
   
    # Busca los enlaces de los videos a diferentes resoluciones en el código javascript
    pag = requests.get(enlaceVideo)
    caldo = BeautifulSoup(pag.content, 'html.parser')
    res = caldo.find("script", {"type":"text/javascript"})
    resol = [] #lista de enlaces a diferentes resoluciones
    resEnCadena = str(res)
    for l in resEnCadena.split('\n'):
        linea = l.strip()
        # Busca los enlaces de flujos de videos (líneas que contienen "file" y no "Download")
        if ('file' in linea) and ('Download' not in linea):
            com = linea.find("http") # comienzo del url
            fin = linea.find('"', com + 1) # fin del url
            nl = linea[com:fin] # url usable
            resol.append(nl) # se agrega a la lista de enlaces
    enlaceVideo = resol.pop() # máxima calidad (último de la lista)
    # enlaceVideo = resol[0] # mínima calidad (primero de la lista)
    
    # Crea un item reproducible a partir del enlace al video.
    itemAreproducir = xbmcgui.ListItem(path=enlaceVideo)
    
    # Pasa el item al reproductor de Kodi.
    xbmcplugin.setResolvedUrl(ID, True, listitem=itemAreproducir)


if __name__ == '__main__':
    main()
